.. The Screenless Office documentation master file, created by
   sphinx-quickstart on Tue Apr  7 09:02:46 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The Screenless Office
=================================================

Contents:

.. toctree::
   :maxdepth: 2

   index
   installing
   working
   extending


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

