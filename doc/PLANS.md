==Phase I Features==

- Reading, Navigating and Publishing Documents - Publications Dept.
  - Print RSS newspaper - "Yesterday's Tomorrows"
  - Print Articles (any HTML page) with links as footnotes + barcodes
  - document-cam-based CMS

- Reading and Writing Email
  - fetch and print a list of unread mails
  - Delete or mark as spam
  - print mail (with menu of options)
  - 
