import random
import string

import barcode

chars = string.ascii_letters + string.digits
for code in range(30):
    code = ''.join(random.choice(chars) for _ in range(5))
    code = "SAp." + code
    out_file = "/tmp/" + code + ".svg"
    barcode.generate("code128", code, output=out_file)
