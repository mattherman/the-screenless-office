import vlc

from bureau import Bureau, add_command


class Audio(Bureau):
    """
    The Audio Services department provides for playback and recording
    of sound within the office environment.
    """

    name = "Audio Services Dept."
    prefix = "AU"
    version = 0

    def __init__(self):
        Bureau.__init__(self)

        self.urldb = self.open_db("urldb")
        self.player = vlc.MediaPlayer()
        
    @add_command("p", "Play an album, track or a live stream.")
    def play(self, data):
        """
        Initiates playback of a media reference. This could be a song or album
        stored on the local office or remote URLs for live playback. Currently,
        only supports line-out signals on the default DAC.
        """
        shortcode, _ = data.split(".")
        self.log.debug("looking up shortcode " + shortcode)
        url = self.urldb.get(shortcode)
        self.log.debug(" playing url " + url)

        self.player.set_mrl(url)
        self.player.play()

    @add_command("stop", "Halt audio playback.")
    def stop(self):
        """
        Stops all audio currently playing audio output.
        """
        self.player.pause()

    @add_command("resu", "Resume playback.")
    def resume(self):
        """
        Resume playback of paused audio.
        """
        self.player.play()

    @add_command("next", "Play the next song.")
    def play_next(self):
        """
        Skip to the next song in the playlist or album.
        """
        #subprocess.call(["mocp", "-f"])
        # TODO
        pass

    @add_command("prev", "Play the previous song.")
    def play_prev(self):
        """
        Skip to the previous song in the playlist or album.
        """
        #subprocess.call(["mocp", "-r"])
        # TODO
        pass

    @add_command("nowp", "Now Playing")
    def now_playing(self):
        """
        Prints the currently playing song or stream on the small printer.
        """
        #out = subprocess.check_output(["mocp", "-i"]).decode("utf-8")
        # TODO: sort out how to do this with 
        out = "Now Playing: "
        out += self.player.get_media().get_meta(vlc.Meta.Title) + "\n"
        nowplaying = self.player.get_media().get_meta(vlc.Meta.NowPlaying)
        if nowplaying == "":
            out += "by " + self.player.get_media().get_meta(vlc.Meta.Title) + "\n"
            out += "from the album '" + self.player.get_media().get_meta(vlc.Meta.Album) \
                    + "'\n"
        else:
            out += nowplaying + "\n"

        self.log.debug("info output:" + out)
        self.print_small(out)

    def save_url(self, url):
        """
        saves an url for a local file or network audio stream.
        """
        code = self.urldb.store_and_get_shortcode(url)
        print("saved url with shortcode: ", code)

        
def main():
    au = Audio()
    au.run()


if __name__ == "__main__":
    main()
