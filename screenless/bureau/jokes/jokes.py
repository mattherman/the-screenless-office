import glob
import math
import mimetypes
import os
import subprocess
import urllib

from PIL import Image

from bureau import Bureau, add_command, add_api


class Humor(Bureau):
    """
    This bureau entertains the modern worker and provides colorful
    bons mots for managers who need to warm up an audience.
    """

    name = "Department of Humor"
    prefix = "HA"
    version = 0

    def __init__(self):
        Bureau.__init__(self)

    @add_command("joke", "Fortune Cookie")
    def print_fortune(self):
        """
        Prints a clever quip.
        """
        jux = str(subprocess.check_output("/usr/games/fortune"), encoding="UTF-8")
        self.print_small(jux)

    @add_api("gif", "Moving Picture")
    def print_gif(self, data):
        """
        Prints out a series of image frames which can be viewed in lively
        motion on any standard zoetrope. (Ø200mm)
        """
        # download the video file
        d_url = data["url"]
        filename, headers = urllib.request.urlretrieve(d_url)
        print("fetching", d_url, filename)

        # make sure we have a legit filename
        ext = mimetypes.guess_extension(headers["Content-Type"])
        if not filename.endswith(ext):
            os.rename(filename, filename + ext)
            filename = filename + ext
            print("renamed to ", filename)

        # if we have something that's a gif or webp (png?) then 
        #  just print 12 frames
        if filename.endswith(("gif", "webp")):
            print("gif stuff")
            img = Image.open(filename)
            print(img.n_frames)
            grab_frame = 0
            out_frame = 0
            in_len = float(img.n_frames)
            # TODO: deal with frame counts lower than 12
            #       and maybe a different algo that includes endpoints
            for frame in range(img.n_frames):
                img.seek(frame)
                if grab_frame == frame:
                    img_rotated = img.rotate(90, expand=True)
                    self.print_small_image(img_rotated)
                    out_frame += 1
                    grab_frame = math.ceil(out_frame * in_len / 12)
        else:
            # how many frames do we have?
            cli = "ffprobe -i " + filename + \
                  " -show_format -v quiet | sed -n 's/duration=//p'"
            vid_len = str(subprocess.check_output(cli), encoding="UTF-8")
            print("video len: " + vid_len)
            # TODO: if vid_len is not a number handle this error

            # lengthen/shorten it to 12 frames 
            # dump frames to temp files
            cli = "ffmpeg -i" + filename + " -vf fps=12/" + vid_len +\
                  " thumb%02d.png"

            # print em out!

        self.print_small("")


def main():
    ha = Humor()
    ha.run()


if __name__ == "__main__":
    main()
