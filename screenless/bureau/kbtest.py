import os
import readline
import time

import zmq

ctx = zmq.Context()

basepath = os.path.expanduser("~/.screenless/")
last = ""

while True:
    msg = input("> ")
    send = ctx.socket(zmq.REQ)
    send.connect("ipc://" + basepath + msg[0:2] + ".ipc")
    send.send_string(msg[2:])
    print("sending ", msg[2:])
    ret = None
    trys = 0
    while (not ret) and (trys < 200):
        try:
            ret = send.recv(flags=zmq.NOBLOCK)
            print("got", ret)
        except zmq.ZMQError:
            trys += 1
            time.sleep(0.05)
    if not ret:
        print("Error: timeout after 10 sec")
        send.close()
